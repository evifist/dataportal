# DataPortal

This is an open source BIM data exchange, modification and visualization tool

It was started for my graduation work while doing the C.A.S in BIM Coordination at the HES-SO (Fribourg, Switzerland)

## Built using
Here's a list of all the amazing open projects that are being used or helped to build DataPortal

* Bulma.io
* Flask
* Datatables.net
* IFC.js (web-ifc-viewer)
* IfcOpenShell
* jsTree
* jQuery

## Requirements
Here's a list of dependencies that are needed to run DataPortal

* Python 3.9+
* MySQL
* npm (install Node.js on Windows)
* git
* bash
* IfcOpenShell
* IfcOpenShell-Python API


## Installation

* First, Clone this repository

The easiest way to install is using Docker.

If you want to install it without Docker, see below.

### Using Docker

* Install [Docker](https://docs.docker.com/get-docker/)
* run a terminal at main directory
* `cd docker`
* `docker-compose up`

The docker-compose can take up to 5 minutes to build the first time.

When the application runs, you can connect using your browser at [localhost:5000](http://localhost:5000)


### Local installation
To install the application locally, you can follow the steps below

### Install requirements
install the requirements.txt dependencies

### Create MySQL Database
Install MySQL

Create database with script in `scripts/create_database.sql`

### Create 3dviewer JS files

Install node.js from https://nodejs.org/en/

Install Git for windows: https://gitforwindows.org/ to use git-bash

Build 3dviewer JS:

Run git bash at DataPortal main directory location to use npm

```
cd 3dviewer
npm i
npm run build
```

### Install IfcOpenShell-Python Modules
Download latest IfcOpenShell-Python version from : http://ifcopenshell.org/python

Extract downloaded archive and copy `ifcopenshell` directory to your local python installation site-packages directory

(on Windows:`%userprofile%\appdata\Local\Programs\Python\Python39\Lib\site-packages`)

### Install IfcOpenShell-Python API
Clone/Download IfcOpenShell repository from: https://github.com/IfcOpenShell/IfcOpenShell

Copy `api` and `util` directories from `src/ifcopenshell-python/ifcopenshell/` folder to your local ifcOpenShell-Python install in site-packages

(on Windows:`%userprofile%\appdata\Local\Programs\Python\Python39\Lib\site-packages\ifcopenshell`)

### Run the app.py application
Use a terminal to run app.py using Python
When the application runs, you can connect using your browser at [localhost:5000](http://localhost:5000)