from yaml import load

import os

try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper

# default path is in:
# On Windows: %USERPROFILE%\.config\dataportal\dataportal.yml
# On Unix: ~/.config/dataportal/dataportal.yml
CONFIG_FILE_PATH = os.path.join(os.path.expanduser("~"), ".config", "dataportal", "dataportal.yml")

# If the config yaml file is not in the default path, the fallback config file path is in the current working directory
FALLBACK_FILE_PATH = "./dataportal.yml"


# Try to load the config file from the config file path, else it tries in the current working directory
def get_config_from_file():
    try:
        with open(CONFIG_FILE_PATH) as f:
            data = load(f, Loader=Loader)
    except FileNotFoundError:
        with open(FALLBACK_FILE_PATH) as f:
            data = load(f, Loader=Loader)


    return data
