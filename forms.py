# -----------------------------------------------
# imports
# -----------------------------------------------

from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileRequired, FileAllowed
from werkzeug.utils import secure_filename
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired


class SignUpForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired()])
    firstname = StringField("First name")
    lastname = StringField("Last name")
    organization = StringField("Organization")
    role = StringField("Role")
    password = PasswordField("Password")
    password2 = PasswordField("Re-type password")


class LoginForm(FlaskForm):
    email = StringField("Email", validators=[DataRequired()])
    password = PasswordField("Password")


class ImportForm(FlaskForm):
    file = FileField("File", validators=[FileRequired(), FileAllowed(["ifc", "xls", "xlsx"])])
