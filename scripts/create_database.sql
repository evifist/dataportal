/*
 Run this SQL script to create the sample DB for DataPortal on your MySQL server
 Version 2021.08.15 - Only stores users with sign in data and use it back for login/authentication
 */
create database if not exists dataportal;
use dataportal;
create table if not exists user (
    id int not null auto_increment,
    email varchar(200) not null,
    firstname varchar(100),
    lastname varchar(100),
    organization varchar(100),
    role varchar(100),
    password varchar(200) not null,
    primary key (id)
);
