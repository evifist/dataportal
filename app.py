# -----------------------------------------------
# DATAPORTAL  - Sven 2021
# -----------------------------------------------

# -----------------------------------------------
# imports
# -----------------------------------------------

import os
import pathlib
import datetime
import ifcopenshell
import ifcopenshell.api
import mimetypes

mimetypes.add_type('application/javascript', '.js')


from flask import Flask, render_template, redirect, flash, url_for, send_from_directory, Response, request
from flask_login import LoginManager, login_user, logout_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename

from database import db
from database import User
from forms import SignUpForm, LoginForm, ImportForm
from models import ImportedFile, Property, PropertySet

# -----------------------------------------------
# App Code here
# -----------------------------------------------

# To run the app, you have to install flask, then set your flask app and run it
# on Windows could be by using: 'FLASK_APP=app.py flask run'

app = Flask(__name__)

pset_separator = "__a__"

# -----------------------------------------------
# Login manager
# -----------------------------------------------

login_manager = LoginManager()
login_manager.login_message = "Please log in to access this page."
login_manager.login_message_category = "info"
login_manager.init_app(app)


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


app.secret_key = os.urandom(16)


# -----------------------------------------------
# IfcOpenShell
# -----------------------------------------------


# -----------------------------------------------
# Utils
# -----------------------------------------------


# From stackoverflow: https://stackoverflow.com/questions/1094841/get-human-readable-version-of-file-size
def sizeof_fmt(num, suffix='B'):
    for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
        if abs(num) < 1024.0:
            return "%3.1f%s%s" % (num, unit, suffix)
        num /= 1024.0
    return "%.1f%s%s" % (num, 'Yi', suffix)


# -----------------------------------------------
# Routing
# -----------------------------------------------

@app.route("/", methods=["GET"])
def get_root():
    return render_template("public/Index.html")


@app.route("/import", methods=["GET"])
@login_required
def get_import():
    form = ImportForm()
    return render_template("user/Import.html", form=form)


@app.route("/import", methods=["POST"])
@login_required
def post_import():
    form = ImportForm()

    if form.validate():
        f = form.file.data
        filename = secure_filename(f.filename)
        _, file_extension = os.path.splitext(filename)
        # Remove dot at the beginning of extension
        file_extension = file_extension[1:len(file_extension)]
        # If xlsx, merge with xls files directory
        if file_extension == "xlsx":
            file_extension = "xls"
        # Create the filepath to save the file
        upload_dir = "uploads"
        # Create directory if needed
        os.makedirs(os.path.join(upload_dir, file_extension), exist_ok=True)

        output_path = os.path.join(upload_dir, file_extension, filename)
        # Save the file in the directory
        f.save(output_path)
        flash("File successfully imported! ", "success")
        return redirect(url_for("get_import"))

    flash("import went wrong. ", "warning")
    return redirect(url_for("get_import"))


@app.route("/files", methods=["GET"])
@login_required
def get_imported_files():
    # List all files in uploads
    imported_files = pathlib.Path("uploads").glob("**/*")
    # List only files and not sub-directories
    temp_imported_files = []
    for file in imported_files:
        if file.is_file():
            file_name = file.name
            file_stat = file.stat()
            # Convert second datetime in a string with YYYY-MM-DD format
            file_date = datetime.datetime.fromtimestamp(file_stat.st_mtime).strftime("%Y-%m-%d")
            file_size = file_stat.st_size
            file_size = sizeof_fmt(file_size)
            _, file_type = os.path.splitext(file_name)
            file_type = file_type[1:len(file_type)]
            if file_type == "xlsx":
                file_type = "xls"

            file_path = str(file)
            temp_imported_files.append(ImportedFile(file_name, file_date, file_type, file_size, file_path))
    imported_files = temp_imported_files

    return render_template("user/Files.html", imported_files=imported_files)


@app.route("/download/<path:file_path>", methods=["GET"])
@login_required
def get_download(file_path):
    # Replace blackslash to make it work on Windows
    file_path = file_path.replace("\\", "/")
    file_path = file_path[len("uploads/"):]

    return send_from_directory("uploads", file_path, as_attachment=True)


@app.route("/delete/<path:file_path>", methods=["POST"])
@login_required
def post_delete_file(file_path):
    real_path = os.path.realpath(file_path)
    # Test if the file we want to delete is really in the uploads folder
    if real_path.startswith(os.path.realpath("uploads")) and os.path.exists(real_path):
        os.unlink(file_path)
        flash("File successfully deleted! ", "success")
        return redirect(url_for("get_imported_files"))

    flash("Couldn't delete file! ", "warning")
    return redirect(url_for("get_imported_files"))


@app.route("/export", methods=["GET"])
@login_required
def get_export():
    return render_template("user/Export.html")


@app.route("/editifc/<path:file_path>", methods=["GET"])
@login_required
def get_editifc(file_path):
    # Replace blackslash to make it work on Windows
    file_path = file_path.replace("\\", "/")
    real_path = os.path.realpath(file_path)
    file_name = os.path.basename(file_path)

    # Test if the file we want to edit is really in the uploads folder
    if real_path.startswith(os.path.realpath("uploads")) and os.path.exists(real_path):
        ifc_url = os.path.join("/download/", file_path)

        ifc_file = ifcopenshell.open(file_path)
        walls = ifc_file.by_type("IfcWall")
        slabs = ifc_file.by_type("IfcSlab")
        entities = list(walls) + list(slabs)

        return render_template("user/EditIfc.html", ifc_url=ifc_url, file_name=file_name, entities=entities)

    flash("Failed to load IFC", "danger")
    return redirect(url_for("get_imported_files"))


@app.route("/ifcelementdetails/<string:file_name>/<string:ifcGlobalId>", methods=["GET"])
@login_required
def get_ifc_element_details(file_name, ifcGlobalId):
    file_path = os.path.join("uploads", "ifc", file_name)

    ifc_file = ifcopenshell.open(file_path)
    elem = ifc_file.by_guid(ifcGlobalId)

    # ifc get properties logic
    # w.IsDefinedBy[0-n] -> RelatingPropertyDefinition.Name ==> name of propertyset
    # w.IsDefinedBy[0-n] -> RelatingPropertyDefinition -> HasProperties[0-n] ==> Name + NominalValue.wrappedValue

    property_sets = []
    for pset in elem.IsDefinedBy:
        if hasattr(pset, "RelatingPropertyDefinition"):
            pset_name = pset.RelatingPropertyDefinition.Name
            properties = []

            rpd = pset.RelatingPropertyDefinition

            if hasattr(rpd, "HasProperties"):
                for property in pset.RelatingPropertyDefinition.HasProperties:
                    property_name = property.Name
                    property_value = property.NominalValue.wrappedValue
                    datatype = type(property_value).__name__
                    my_new_property = Property(property_name, property_value, datatype)
                    properties.append(my_new_property)
            if hasattr(rpd, "Quantities"):
                for property in pset.RelatingPropertyDefinition.Quantities:
                    property_name = property.Name
                    value_name = ""
                    for pname in dir(property):
                        if pname.endswith("Value"):
                            value_name = pname
                    property_value = getattr(property, value_name)
                    datatype = type(property_value).__name__
                    my_new_quantity = Property(property_name, property_value, datatype)
                    properties.append(my_new_quantity)

            my_new_propertyset = PropertySet(pset_name, properties)
            property_sets.append(my_new_propertyset)

    return render_template("user/ifc_element_details.html", property_sets=property_sets, file_name=file_name,
                           ifcGlobalId=ifcGlobalId, pset_separator=pset_separator)


@app.route("/saveifc/<string:file_name>/<string:ifcGlobalId>", methods=["POST"])
@login_required
def post_save_ifc(file_name, ifcGlobalId):
    file_path = os.path.join("uploads", "ifc", file_name)

    ifc_file = ifcopenshell.open(file_path)
    elem = ifc_file.by_guid(ifcGlobalId)

    output_file_name = request.form["output_file_name"]

    # construct updated properties structure
    property_sets = {}

    processed_psets = []

    for key in request.form:
        value = request.form[key]
        # convert value to correct type
        if pset_separator in key:
            datatype, pset_name, property_name = key.split(pset_separator)
            if datatype == "bool":
                if value == "false":
                    value = False
                elif value == "true":
                    value = True
                else:
                    value = False  # just in case someone sends invalid data
            elif datatype == "int":
                value = int(value)
            elif datatype == "float":
                value = float(value)

            # add value to structure
            if not pset_name in property_sets:
                property_sets[pset_name] = {}

            pset = property_sets[pset_name]
            pset[property_name] = value

    # update ifc with updated properties
    for pset in elem.IsDefinedBy:
        if hasattr(pset, "RelatingPropertyDefinition"):
            pset_name = pset.RelatingPropertyDefinition.Name
            rpd = pset.RelatingPropertyDefinition
            if rpd.is_a("IfcPropertySet"):
                if pset_name in property_sets:
                    pset_properties = property_sets[pset_name]
                    ifcopenshell.api.run("pset.edit_pset", ifc_file, pset=rpd,
                                             properties=pset_properties)
                    processed_psets.append(pset_name)

    # add new psets
    for pset_name, pset_properties in property_sets.items():
        if pset_name not in processed_psets:
            new_pset = ifcopenshell.api.run("pset.add_pset", ifc_file, product=elem, name=pset_name)
            # Add properties and values
            ifcopenshell.api.run("pset.edit_pset", ifc_file, pset=new_pset,
                                 properties=pset_properties)

    # write output file
    output_file_path = os.path.join("uploads", "ifc", output_file_name)
    ifc_file.write(output_file_path)

    flash("IFC file successfully saved! ", "success")
    return redirect(url_for("get_editifc", file_path=output_file_path))

def parse_ifc_items(file_path):
    ifc_file = ifcopenshell.open(file_path)

    items = []

    projects = ifc_file.by_type("IfcProject")
    for p in projects:
        items.append(p)
        for e in ifc_file.traverse(p):
            items.append(e)

    return items


@app.route("/3dviewer/<path:file_path>", methods=["GET"])
@login_required
def get_3dviewer(file_path):
    file_name = os.path.basename(file_path)
    return render_template("user/3dviewer.html", file_path=file_path, file_name=file_name)


@app.route("/3dviewerjs/<path:file_path>", methods=["GET"])
@login_required
def get_js(file_path):
    # Replace blackslash to make it work on Windows
    file_path = file_path.replace("\\", "/")
    real_path = os.path.realpath(file_path)

    # Test if the file we want to see in 3D is really in the uploads folder
    if real_path.startswith(os.path.realpath("uploads")) and os.path.exists(real_path):
        ifc_url = os.path.join("/download/", file_path)
        js = render_template("/js/3dviewer.js", ifc_url=ifc_url)
        return Response(js, mimetype="application/javascript")

    flash("Failed to load IFC", "danger")
    return redirect(url_for("get_imported_files"))


@app.route("/documentation", methods=["GET"])
def get_documentation():
    return render_template("public/Documentation.html")


@app.route("/about", methods=["GET"])
def get_about():
    return render_template("public/About.html")


@app.route("/contact", methods=["GET"])
def get_contact():
    return render_template("public/Contact.html")


@app.route("/signup", methods=["GET"])
def get_signup():
    form = SignUpForm()
    return render_template("public/Signup.html", form=form)


@app.route("/signup", methods=["POST"])
def post_signup():
    form = SignUpForm()
    if form.validate():
        data = form.data
        if data["password"] == data["password2"]:
            # Hash and Salt password
            password_hash = generate_password_hash(data["password"])
            data["password"] = password_hash

            user = User(**data)
            user.save()
            flash("Successfully signed up! ", "success")
            return redirect("/")

    flash("Sign up went wrong. ", "warning")
    return render_template("public/Signup.html", form=form)


@app.route("/login", methods=["GET"])
def get_login():
    form = LoginForm()
    return render_template("public/Login.html", form=form)


@app.route("/login", methods=["POST"])
def post_login():
    form = LoginForm()
    if form.validate():
        data = form.data
        email = data["email"]
        password = data["password"]
        user = User.get(email == email)

        # If exists and password matches
        if user is not None and check_password_hash(user.password, password):
            login_user(user)

            flash("Successfully logged in! ", "success")
            return redirect("/")

    flash("Log in went wrong. ", "warning")
    return render_template("public/Login.html", form=form)


@app.route("/logout", methods=["GET"])
def get_logout():
    logout_user()
    flash("Successfully logged out! ", "info")
    return redirect("/")


def main():
    app.run(host="0.0.0.0", port="5000", debug=True)


# -----------------------------------------------
# Request hooks
# -----------------------------------------------

# This hook ensures that a connection is opened to handle any queries
# generated by the request.
@app.before_request
def _db_connect():
    db.connect(reuse_if_open=True)


# This hook ensures that the connection is closed when we've finished
# processing the request.
@app.teardown_request
def _db_close(exc):
    if not db.is_closed():
        db.close()


if __name__ == "__main__":
    main()
