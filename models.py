import dataclasses


@dataclasses.dataclass
class ImportedFile:
    name: str
    date: str
    type: str
    size: int
    path: str

@dataclasses.dataclass
class Property:
    key: str
    value: str
    datatype: str

@dataclasses.dataclass
class PropertySet:
    name: str
    properties: list[Property]